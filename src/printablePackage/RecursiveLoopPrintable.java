package printablePackage;

public class RecursiveLoopPrintable {
	public void print(Integer[][] data) {
		printInternal(data);
	}
	private void printInternal(Object data) {
		if(data == null) {
			System.out.println("null");
			return;
		}
		if(data.getClass().isArray()) {
			
			Object[] newData = (Object[]) data;
			if(newData.length < 1) {
				System.out.println("empty");
				return;
			}
			
			for(Object element : newData) {
				printInternal(element);
			}
			System.out.println();
			return;
			
		}
		System.out.print(data + " ");
	}

}
