package printablePackage;

public class SingleLoopPrintableImpl implements MaxtrixPrintable {

	@Override
	public void print(int[][] data) {
		boolean isFinished = false;
		int rowIndex = 0;
		int columnIndex = 0;
		while(!isFinished) {
			if(data.length <= rowIndex) {
				isFinished = true;
				continue;
			}
			int[] row = data[rowIndex];
			if (row == null) {
				System.out.println("null");
				rowIndex++;
				continue;

			}
			if (row.length < 1) {
				System.out.println("empty");
				rowIndex++;
				continue;
			}
			if(row.length <= columnIndex) {
				rowIndex++;
				columnIndex = 0;
				System.out.println();
				continue;
			}
			System.out.print(row[columnIndex++] + " ");
		}

	}

}
