package printablePackage;

public class NestedLoopsPrintableImpl implements MaxtrixPrintable {

	@Override
	public void print(int[][] data) {

		for (int x = 0; x < data.length; x++) {
			if (data[x] == null) {
				System.out.println("null");
				continue;

			}
			if (data[x].length < 1) {
				System.out.println("empty");
				continue;

			}

			for (int y = 0; y < data[x].length; y++) {

				System.out.print(data[x][y] + " ");

			}
			System.out.println();
		}

	}

}
